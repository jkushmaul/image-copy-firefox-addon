browser.contextMenus.create({
    id: "no-really-copy-image",
    title: "Copy image to clipboard, even when hidden",
    contexts: ["image"],
  });

  browser.contextMenus.onClicked.addListener((info, tab) => {
    console.log("Got a context menu action");


    switch (info.menuItemId) {
      case "no-really-copy-image":
        console.log("It was " + info.menuItemId);
        copyImage(info, tab.id);
        break;
    }
  });

  function copyImage(img, tabId) {
      var imgUrl = img.srcUrl;
    
      if (imgUrl.startsWith("http")) {
          copyHttpImage(imgUrl, tabId);
      } else if (imgUrl.startsWith("data:image/jpeg;base64,")){
          copyBase64Image(imgUrl,tabId);
      }
     
  }

  function copyHttpImage(httpImgUrl, tabId) {
    console.log("Handling http URL");
    fetch(httpImgUrl)
        .then(response => response.arrayBuffer())
        .then(buffer =>  browser.clipboard.setImageData(buffer, "png"));
  }
  function _base64ToArrayBuffer(base64) {
    var binary_string = window.atob(base64);
    var len = binary_string.length;
    var bytes = new Uint8Array(len);
    for (var i = 0; i < len; i++) {
        bytes[i] = binary_string.charCodeAt(i);
    }
    return bytes.buffer;
  }

  function copyBase64Image(base64ImgUrl, tabId) {
    console.log("Handling base64 URL");
    //data:image/jpeg;base64,<base64>
    var dataUriMatches = base64ImgUrl.match(/^data:image\/([^;]+);base64,(.*)$/);
    var imageType = dataUriMatches[1];
   
    var buffer = _base64ToArrayBuffer(dataUriMatches[2]);
    browser.clipboard.setImageData(buffer, imageType);
    
  }
 